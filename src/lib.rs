extern crate core;

use async_trait::async_trait;
use serenity::model::application::interaction::{Interaction, InteractionResponseType};
use serenity::model::gateway::Ready;
use serenity::model::prelude::GuildId;
use serenity::prelude::*;

use crate::configuration::{get_configuration, Settings};

pub mod configuration;

struct Handler;

#[async_trait]
impl EventHandler for Handler
{
    async fn ready(&self, ctx: Context, ready: Ready)
    {
        println!("{} is connected (＠＾◡＾)", ready.user.name);

        // TODO: Find a better way of passing settings here
        let settings = get_configuration().expect("Failed to load settings");

        let guild_id = GuildId(settings.discord.guild_id);

        let commands = GuildId::set_application_commands(&guild_id, &ctx.http, |commands| {
            commands.create_application_command(|command| {
                command.name("ping").description("A ping command")
            })
        })
            .await;

        println!(
            "I now have the following guild slash commands: {:#?}",
            commands
        );
    }

    async fn interaction_create(&self, ctx: Context, interaction: Interaction)
    {
        if let Interaction::ApplicationCommand(command) = interaction
        {
            println!("Received command interaction: {:#?}", command);

            let content = match command.data.name.as_str()
            {
                "ping" =>
                    {
                        println!("Received a ping");
                        "Hey, I'm alive. And I love you <@843091303343456286>!!!".to_string()
                    }
                _ => "not implemented :(".to_string(),
            };

            if let Err(why) = command
                .create_interaction_response(&ctx.http, |response| {
                    response
                        .kind(InteractionResponseType::ChannelMessageWithSource)
                        .interaction_response_data(|message| message.content(content))
                })
                .await
            {
                println!("Cannot respond to slash command: {}", why)
            }
        };
    }
}

pub async fn run_chatbot(settings: &Settings) -> Result<(), serenity::Error>
{
    println!("Starting kaibot (￣ρ￣)..zzZZ");

    let mut client = Client::builder(settings.discord.discord_token.as_str(), GatewayIntents::empty())
        .event_handler(Handler)
        .await
        .expect("Error creating client");

    client.start().await
}
