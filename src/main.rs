use kaibot::configuration::get_configuration;
use kaibot::run_chatbot;

#[tokio::main]
async fn main() -> anyhow::Result<()>
{
    let settings = get_configuration()?;

    if let Err(why) = run_chatbot(&settings).await
    {
        eprintln!("Client error: {:?}", why)
    }

    Ok(())
}
