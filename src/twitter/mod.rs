use twitter_v2::authorization::BearerToken;
use twitter_v2::query::TweetField;
use twitter_v2::TwitterApi;

use crate::get_configuration;

pub async fn initialize_twiter() -> anyhow::Result<()>
{
    let settings = get_configuration()?;

    let tweet = TwitterApi::new(generate_auth())
        .get_tweet(1261326399320715264)
        .tweet_fields([TweetField::AuthorId, TweetField::CreatedAt])
        .send()
        .await?
        .into_data()
        .expect("this tweet should exist");
    dbg!("{:#?}", tweet);

    Ok(())
}

fn generate_auth() -> BearerToken
{
    let settings = get_configuration().unwrap();
    BearerToken::new(settings.twitter.bearer_token)
}
